import * as React from 'react'
import { connect } from 'react-redux'
// import { useTranslation } from 'react-i18next'
import { Menu } from 'antd'
import { useHistory } from 'react-router-dom'
import type { FC } from 'react'
import { ApplicationState } from '../../store'
import './styles.scss'

type Props = {}

const HeaderComponent: FC<Props> = () => {
  // const { t } = useTranslation(['lang'])
  const history = useHistory()
  return (
    <div className="header__component container">
      <div className="header--menu">
        <div>
          <Menu mode="horizontal">
            <Menu.Item key="home" onClick={() => history.push('/')}>
              Home
            </Menu.Item>
            <Menu.SubMenu key="SubMenu" title="About">
              <Menu.ItemGroup>
                <Menu.Item key="about:1" onClick={() => history.push('/about')}>
                  About
                </Menu.Item>
                <Menu.Item key="about:2" onClick={() => history.push('/story')}>
                  Story
                </Menu.Item>
              </Menu.ItemGroup>
            </Menu.SubMenu>
          </Menu>
        </div>
        <div>
          <Menu>
            <Menu.Item key="alipay" onClick={() => history.push('/login')}>
              Login
            </Menu.Item>
          </Menu>
        </div>
      </div>
    </div>
  )
}

export default HeaderComponent
