import * as React from 'react'
import { connect } from 'react-redux'
import { RouteComponentProps, withRouter } from 'react-router-dom'
import { withTranslation, WithTranslation } from 'react-i18next'
import { css } from 'emotion'
import { Layout } from 'antd'
import { ApplicationState } from '../../store'
import Sidebar from '../Sidebar'
import Header from '../Header'

interface States { }

interface Props extends WithTranslation {
  children?: any
}

interface PropsFromState { }

interface PropsFromDispatch { }

type AllProps = Props & RouteComponentProps & PropsFromState & PropsFromDispatch

class MainLayout extends React.Component<AllProps, States> {
  constructor(props: Readonly<AllProps>) {
    super(props)

    this.state = {}
  }

  render() {
    const { children } = this.props
    return (
      <Layout className={css`background-color: var(--color-white);`}>
        <Sidebar />
        <Layout className={css`background-color: var(--color-white);`}>
          <Header />
          <Layout.Content className={css`padding-left: 2.75rem; padding-right: 1.5rem;`}>{children}</Layout.Content>
        </Layout>
      </Layout>
    )
  }
}

const mapStateToProps = (state: ApplicationState) => ({})

const mapDispatchToProps = {}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withTranslation('lang')(withRouter(MainLayout)))
