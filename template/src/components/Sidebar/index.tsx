import * as React from 'react'
import { connect } from 'react-redux'
// import { useTranslation } from 'react-i18next'
import { ApplicationState } from '../../store'
import './styles.scss'

interface PropsFromState {}

interface PropsFromDispatch {}

type AllProps = PropsFromState & PropsFromDispatch

const Sidebar: React.FC<AllProps> = (props) => {
  // const { t } = useTranslation(['lang'])
  return (
    <div className="sidebar__component" />
  )
}

const mapStateToProps = (state: ApplicationState) => ({})

const mapDispatchToProps = {}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Sidebar)
