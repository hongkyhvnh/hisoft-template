const isDevEnv = process.env.NODE_ENV === 'development'
const isProductionEnv = process.env.NODE_ENV === 'production'

console.log('🚀 ~ file: config.ts ~ line 2 ~ isDevEnv', isDevEnv)
console.log('🚀 ~ file: config.ts ~ line 6 ~ isProductionEnv', isProductionEnv)

const api = { apiBaseUrl: '' }
// List api for development here
// api.apiBaseUrl = 'https://example.com'
// ...
// ...
// ...

// Builf API enpoint in file .env
if (isProductionEnv) {
  api.apiBaseUrl = process.env.REACT_APP_END_POINT || ''
  console.log('🚀 ~ file: config.ts ~ line 17 ~ process.env', process.env)
}

export default api
