import * as React from 'react'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import { Store } from 'redux'
import preval from 'preval.macro'
import { History } from 'history'
import Routes from './routes'
import { ApplicationState } from './store'
import 'antd/dist/antd.css'
import './styles/main.scss'
import withClearCache from './ClearCache'

interface MainProps {
  store: Store<ApplicationState>
  history: History
}

const dateTimeStamp = preval`module.exports = () => {
  const checkZero = (data) => {
    if(data.length === 1) return data = "0" + data
    return data;
  }
  
  const today = new Date();
  const day = checkZero(new Date().getDate() + "");
  const month = checkZero((today.getMonth() + 1) + "");
  const year = checkZero(today.getFullYear() + "");
  const hour = checkZero(today.getHours() + "");
  const minutes = checkZero(today.getMinutes() + "");
  const seconds = checkZero(today.getSeconds() + "")
  return day + "/" + month + "/" + year + " " + hour + ":" + minutes + ":" + seconds
}`
console.log('=====================================')
console.log('REACT VERSION CREATED AT: ', dateTimeStamp)
console.log('=====================================')
const Main: React.FC<MainProps> = ({ store, history }) => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Routes />
    </ConnectedRouter>
  </Provider>
)
const ClearCacheComponent = withClearCache(Main)
const App: React.FC<MainProps> = ({ store, history }) => (
  <ClearCacheComponent store={store} history={history} />
)

export default App
