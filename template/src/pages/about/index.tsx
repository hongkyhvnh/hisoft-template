import * as React from 'react'
import './styles.scss'

const About = () => (
  <div className="about__page">
    <h1 className="title text-center font-extrabold">ABOUT PAGE</h1>
  </div>
)

export default About
