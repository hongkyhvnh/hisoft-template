import * as React from 'react'
import './styles.scss'

const HomePage = () => (
  <div className="home__page container">
    <h1 className="title">WELCOME TO HISOFT TEAM</h1>
  </div>
)

export default HomePage
