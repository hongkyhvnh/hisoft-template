import * as React from 'react'
import { useDispatch } from 'react-redux'
import {
  Form, Input, Button, message,
} from 'antd'
import './styles.scss'
import { useHistory } from 'react-router'
import { actionLogin } from '../../store/login/actions'

interface PropsFromDispatch {
  getLogin: typeof actionLogin
}

const LoginPage = () => {
  const dispatch = useDispatch()
  const history = useHistory()

  const onLogin = async (values: any) => {
    const data: any = await dispatch(
      actionLogin({
        username: values.username,
        password: values.password,
      })
    )
    if (data) {
      if (data.status === 200) {
        message.success(data.message)
        setTimeout(() => {
          history.push('/list')
        }, 1000)
      } else {
        message.error(data.message)
      }
    }
  }

  return (
    <div className="login__page container">
      <h2 className="title"> Test Unit Form</h2>
      <Form
        layout="horizontal"
        name="basic"
        initialValues={{ remember: true }}
        onFinish={onLogin}
      >
        <Form.Item
          label="Username"
          name="username"
          rules={[{ required: true, message: 'Please input your username!' }]}
        >
          <Input data-cy="username" />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: 'Please input your password!' }]}
        >
          <Input.Password data-cy="password" />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit" data-cy="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  )
}

// getLogin: (data: any) => dispatch(actionLogin(data)),

export default LoginPage
