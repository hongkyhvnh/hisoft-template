import * as React from 'react'
import './styles.scss'

const StoryPage = () => (
  <div className="story__page">
    <h1 className="title text-center font-extrabold">STORY PAGE</h1>
  </div>
)

export default StoryPage
