import * as React from 'react'
import { Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux'
import { RouteComponentProps, withRouter, Redirect } from 'react-router'
import { ApplicationState } from './store'
import MainLayout from './components/MainLayout'
/* PLOP_IMPORT */
import Story from './pages/story'
import About from './pages/about'
import Home from './pages/home'
import Login from './pages/login'
import List from './pages/list'

interface States {}

interface Props {}

interface PropsFromState {}

interface PropsFromDispatch {}

type AllProps = Props & PropsFromState & PropsFromDispatch & RouteComponentProps

class Routes extends React.Component<AllProps, States> {
  constructor(props: Readonly<AllProps>) {
    super(props)

    this.state = {}
  }

  render() {
    return (
      <MainLayout>
        <Switch>
          {/* PLOP_ROUTE */}
          <Route exact path="/story" component={Story} />
          <Route exact path="/about" component={About} />
          <Route exact path="/" component={Home} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/list" component={List} />
          <Redirect to="/" />
        </Switch>
      </MainLayout>
    )
  }
}

const mapStateToProps = (state: ApplicationState) => ({})

const mapDispatchToProps = {}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Routes))
