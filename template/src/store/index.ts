import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import { History } from 'history'
/* PLOP IMPORT STORE */
import storyReducer from './story/reducer'
import storyStates from './story/states'
import aboutReducer from './about/reducer'
import aboutStates from './about/states'
import homeReducer from './home/reducer'
import homeStates from './home/states'
import loginReducer from './login/reducer'
import loginStates from './login/states'
import listReducer from './list/reducer'
import listStates from './list/states'

export interface ApplicationState {
  /* PLOP STORE STATE */
  story: storyStates,
  about: aboutStates,
  home: homeStates,
  login: loginStates,
  list: listStates,
}

export const rootReducer = (history: History) =>
  combineReducers({
    router: connectRouter(history),
    /* PLOP STORE REDUCER */
    story: storyReducer,
    about: aboutReducer,
    home: homeReducer,
    login: loginReducer,
    list: listReducer,
  })
