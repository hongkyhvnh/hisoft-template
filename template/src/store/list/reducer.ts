import { Reducer } from 'redux'
import { Types } from './types'
import States from './states'

const initialState: States = {
  example: [],
}
const ListReducer: Reducer<States> = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state
  }
}
export default ListReducer
