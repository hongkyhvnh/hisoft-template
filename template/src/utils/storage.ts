const loadState = () => {
  try {
    const serializedState: any = sessionStorage.getItem('state')
    if (serializedState === null) {
      return undefined
    }
    return JSON.parse(serializedState)
  } catch (err) {
    return undefined
  }
}

const saveState = (state: any) => {
  try {
    const serializedState = JSON.stringify(state)

    sessionStorage.setItem('state', serializedState)
  } catch {
    //
  }
}

export {
  loadState,
  saveState,
}
